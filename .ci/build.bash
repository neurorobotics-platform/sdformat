#!/bin/bash
set -e
set -o xtrace

whoami
env | sort

# Build 
mkdir -p build && cd build
cmake -DBUILD_TESTING:BOOL=False -DCMAKE_INSTALL_PREFIX=$HOME/.local ..
NPROC=`nproc`
make package -j `expr $NPROC - 2`
