<!-- Fluid Parameters -->
<element name="fluidPamameters" required="0">
  <description>Defines the parameters related to a specific fluid.</description>

    <!-- Viscosity -->
    <element name="viscosity" required="0"> 
      <description>Defines the parameters related to the viscosity of the fluid.</description>
    
      <element name="viscosityMethod" type="int" default="1" required="0">
        <description>Viscosity method
          0: None
          1: Standard
          2: XSPH
          3: Bender and Koschier 2017
          4: Peer et al. 2015
          5: Peer et al. 2016
          6: Takahashi et al. 2015 (improved)
          7: Weiler et al. 2018.</description>
      </element>

      <element name="viscosity" type="double" default="0.01" required="0">
        <description>Coefficient for the viscosity force computation.</description>
      </element>

      <element name="viscoMaxIter" type="int" default="100" required="0">
        <description> (Implicit solvers) Max. iterations of the viscosity solver.</description>
      </element>

      <element name="viscoMaxError" type="double" default="0.01" required="0">
        <description>(Implicit solvers) Max. error of the viscosity solver.</description>
      </element>

      <element name="viscoMaxIterOmega" type="int" default="200" required="0">
        <description>(Peer et al. 2016) Max. iterations of the vorticity diffusion solver.</description>
      </element>

      <element name="viscoMaxErrorOmega" type="double" default="0.01" required="0">
        <description>(Peer et al. 2016) Max. error of the vorticity diffusion solver.</description>
      </element>

      <element name="viscosityBoundary" type="double" default="0.0" required="0">
        <description>(Weiler et al. 2018) Coefficient for the viscosity force computation at the boundary.</description>
      </element>
    
    </element>
    <!-- End Viscosity -->

    <!-- Vorticity -->
    <element name="vorticity" required="0"> 
      <description>Defines the parameters related to the vorticity of the fluid.</description>
    
      <element name="vorticityMethod" type="int" default="0" required="0">
        <description>Vorticity method
          0: None
          1: Micropolar model
          2: Vorticity confinement.</description>
      </element>

      <element name="vorticity" type="double" default="0.01" required="0">
        <description>Coefficient for the vorticity force computation.</description>
      </element>

      <element name="viscosityOmega" type="double" default="0.1" required="0">
        <description>(Micropolar model) Viscosity coefficient for the angular velocity field.</description>
      </element>

      <element name="inertiaInverse" type="double" default="0.5" required="0">
        <description>(Micropolar model) Inverse microinertia used in the micropolar model.</description>
      </element>
    
    </element> 
    <!-- End Vorticity -->

    <!-- Drag Force -->
    <element name="dragForce" required="0"> 
      <description>Drag force related parameters.</description>
    
      <element name="dragMethod" type="int" default="0" required="0">
        <description>Drag force method
          0: None
          1: Macklin et al. 2014
          2: Gissler et al. 2017</description>
      </element>

      <element name="drag" type="double" default="0.01" required="0">
        <description>Coefficient for the drag force computation.</description>
      </element>   
 
    </element> 
    <!-- End Drag Force -->

    <!-- Surface Tension -->
    <element name="surfaceTension" required="0"> 
      <description>Surface tension related parameters.</description>
    
      <element name="surfaceTensionMethod" type="int" default="0" required="0">
        <description>Surface tension method
          0: None
          1: Becker and Teschner 2007
          2: Akinci et al. 2013
          3: He et al. 2014</description>
      </element>

      <element name="surfaceTension" type="double" default="0.05" required="0">
        <description>Coefficient for the surface tension computation.</description>
      </element>   
 
    </element> 
    <!-- End Surface Tension -->

    <!-- Elasticity -->
    <element name="elasticity" required="0"> 
      <description>Elasticity related parameters.</description>
    
      <element name="elasticityMethod" type="int" default="0" required="0">
        <description>Surface tension method
          0: None
          1: Becker et al. 2009
          2: Peer et al. 2018</description>
      </element>

      <element name="youngsModulus" type="double" default="100000.0" required="0">
        <description>Young's modulus - coefficient for the stiffness of the material.</description>
      </element>   
      
      <element name="poissonsRatio" type="double" default="0.3" required="0">
        <description> Poisson's ratio - measure of the Poisson effect.</description>
      </element> 

      <element name="alpha" type="double" default="0.0" required="0">
        <description>Coefficent for zero-energy modes suppression method.</description>
      </element> 

      <element name="elasticityMaxIter" type="int" default="100" required="0">
        <description>(Peer et al. 2018) Maximum solver iterations.</description>
      </element> 

      <element name="elasticityMaxError" type="double" default="1e-4" required="0">
        <description>(Peer et al. 2019) Maximum elasticity error allowed by the solver.</description>
      </element> 
 
    </element> 
    <!-- End Elasticity -->

    <!-- Emmiters -->
    <element name="emmiters" required="0"> 
      <description>Emmiters related parameters.</description>
    
      <element name="maxEmitterParticles" type="int" default="1000" required="0">
        <description>Maximum number of particles the emitter generates. Note that reused particles are not counted here.</description>
      </element>   
      
      <element name="emitterReuseParticles" type="bool" default="false" required="0">
        <description>Reuse particles if they are outside of the bounding box defined by emitterBoxMin, emitterBoxMax.</description>
      </element> 

      <element name="emitterBoxMin" type="vector3" default="-1.0 -1.0 -1.0" required="0">
        <description>Minimum coordinates of an axis-aligned box (used in combination with emitterReuseParticles).</description>
      </element> 

      <element name="emitterBoxMax" type="vector3" default="1.0 1.0 1.0" required="0">
        <description>Maximum coordinates of an axis-aligned box (used in combination with emitterReuseParticles).</description>
      </element> 

    </element> 
    <!-- End Emmiters -->

</element> 
<!-- End Fluid Parameters -->